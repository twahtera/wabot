#[macro_use]
extern crate lazy_static;
extern crate rand;
#[macro_use]
extern crate itertools;

use hlt::command::Command;
use hlt::direction::Direction;
use hlt::game::Game;
use hlt::log::Log;
use hlt::navi::Navi;
use hlt::position::Position;
use hlt::ShipId;

use rand::Rng;
use rand::SeedableRng;
use rand::XorShiftRng;
use std::env;
use std::iter;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;
use std::collections::HashMap;

mod hlt;

fn main() {
    let args: Vec<String> = env::args().collect();
    let rng_seed: u64 = if args.len() > 1 {
        args[1].parse().unwrap()
    } else {
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs()
    };
    let seed_bytes: Vec<u8> = (0..16)
        .map(|x| ((rng_seed >> (x % 8)) & 0xFF) as u8)
        .collect();
    let mut rng: XorShiftRng = SeedableRng::from_seed([
        seed_bytes[0],
        seed_bytes[1],
        seed_bytes[2],
        seed_bytes[3],
        seed_bytes[4],
        seed_bytes[5],
        seed_bytes[6],
        seed_bytes[7],
        seed_bytes[8],
        seed_bytes[9],
        seed_bytes[10],
        seed_bytes[11],
        seed_bytes[12],
        seed_bytes[13],
        seed_bytes[14],
        seed_bytes[15],
    ]);

    let mut game = Game::new();
    let mut navi = Navi::new(game.map.width, game.map.height);
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    Game::ready("Wabot");

    Log::log(&format!(
        "Successfully created bot! My Player ID is {}. Bot rng seed is {}.",
        game.my_id.0, rng_seed
    ));

    let mut ship_returning : HashMap<ShipId, bool> = HashMap::new();

    loop {
        let turns_left = game.constants.max_turns as i32 - game.turn_number as i32;
        
        game.update_frame();
        navi.update_frame(&game);

        let me = &game.players[game.my_id.0];
        let map = &game.map;

        let dropoffs = me
            .dropoff_ids
            .iter()
            .map(|id| game.dropoffs.get(id))
            .flatten();
        let drop_locations: Vec<Position> = dropoffs
            .map(|d| d.position)
            .chain(iter::once(me.shipyard.position))
            .collect();

        let mut command_queue: Vec<Command> = Vec::new();

        for ship_id in &me.ship_ids {
            let ship = &game.ships[ship_id];
            let cell = map.at_entity(ship);

            let closest_drop_location = drop_locations
                .iter()
                .min_by_key(|l| map.calculate_distance(&cell.position, l))
                .unwrap_or(&me.shipyard.position);

            let distance_to_closest_drop = map.calculate_distance(&ship.position, closest_drop_location);

            // if map.calculate_distance(&cell.position, closest_drop_location) > 20 && me.halite >= game.constants.dropoff_cost {
            //     ship.make_dropoff();
            //     command_queue.push(ship.make_dropoff());
            //     continue;
            // }

            // update ship status
            if ship.is_almost_empty() {
                ship_returning.insert(ship.id, false);
            }

            // decide action for ship
            let direction = if ship.halite < cell.halite / 10 {
                Direction::Still
            } else if turns_left - (distance_to_closest_drop as i32) < 12  {
                // Return home at end of game
                if distance_to_closest_drop == 1 {
                    *navi.get_unsafe_moves(&ship.position, &closest_drop_location, true)
                        .get(0).unwrap_or(&Direction::Still)
                    
                } else {
                    navi.naive_navigate(ship, &closest_drop_location, rng.gen())
                }
            } else if *ship_returning.get(&ship.id).unwrap_or(&false) {
                navi.naive_navigate(ship, &closest_drop_location, rng.gen())
            } else if ship.is_almost_full() {
                // Return home when we have lots of cargo
                ship_returning.insert(ship.id, true);
                navi.naive_navigate(ship, &closest_drop_location, rng.gen())
            } else if cell.halite < game.constants.max_halite / 10 {
                // find direction with most halite
                let directions = Direction::get_all_cardinals();
                let random_direction = directions[rng.gen_range(0, 4)];
                let random_position = &ship.position.directional_offset(random_direction);

                let dirs = directions.iter().chain(iter::once(&Direction::Still));
                let radius = navi.radius(&ship.position, 1);


                let best_pos = radius
                    .iter()
                    .max_by_key(|pos| map.at_position(&pos).halite)
                    .unwrap_or(random_position);

                if navi.is_safe(&best_pos) {
                    navi.naive_navigate(&ship, &best_pos, rng.gen())
                } else {
                    let rotated_move = navi.get_unsafe_moves(&ship.position, &best_pos, true)
                        .get(0).unwrap_or(&Direction::Still).rotate_clockwise();
                    if navi.is_safe(&ship.position.directional_offset(rotated_move)) {rotated_move}
                    else {Direction::Still}
                }
            } else {
                Direction::Still
            };

            let newpos = ship.position.directional_offset(direction);
            navi.mark_safe_ship(ship);
            navi.mark_unsafe(&newpos, ship.id);
            command_queue.push(ship.move_ship(direction));
        }

        if game.turn_number <= 200
            && me.halite >= game.constants.ship_cost
            && navi.is_safe(&me.shipyard.position)
        {
            command_queue.push(me.shipyard.spawn());
        }

        Game::end_turn(&command_queue);
    }
}
