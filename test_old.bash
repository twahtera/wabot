#!/usr/bin/env bash

cargo build --release

cd ../wabot2
cargo build --release

cd ../wabot

parallel -q -j 7 ./halite --seed {} --replay-directory replays/ -vvv --width 64 --height 64 "RUST_BACKTRACE=1 ./target/release/my_bot {}" "RUST_BACKTRACE=1 ../wabot2/target/release/my_bot {}" ::: $(for i in $(seq 1 $1); do echo $RANDOM ;done) 2>&1 |grep "Player 0, 'Wabot', was rank 1"


